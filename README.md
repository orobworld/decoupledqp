# Decoupled Bilinear QP #

### What is this repository for? ###

This code addresses a [question](https://or.stackexchange.com/questions/7806/how-to-handle-a-non-separable-bilinear-objective-function-in-the-special-case-of) posted on OR Stack Exchange. The author describes a large scale optimization problem with a bilinear objective function and linear constraints. The (continuous, bounded) variables split into two vectors, *x* and *y*, such that the bilinear objective terms involve products of an *x* and a *y* and, critically, none of the constraints mixes *x* and *y* variables. The author sought to exploit the fact that the constraints were separate for *x* and for *y* (the "decoupling").

The program tests a solution approach that alternates between solving linear programs involving *x* and linear programs involving *y*. It generates random test problems, applies the alternating LP heuristic, and as a benchmark also tries to solve the full quadratic program to global optimality, using a parameter setting for CPLEX (the solver) that results in CPLEX changing the problem to a mixed integer quadratic program.

### Details ###

The code was developed using  a beta version of CPLEX 22.1 but will run with most recent versions.

More details, and some test results, can be found in my [blog post](https://orinanobworld.blogspot.com/2022/02/a-decoupled-quadratic-program.html).

### License ###
The code here is copyrighted by Paul A. Rubin (yours truly) and licensed under a [Creative Commons Attribution 3.0 Unported License](http://creativecommons.org/licenses/by/3.0/deed.en_US). 

