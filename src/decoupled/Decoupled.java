package decoupled;

import problem.Problem;
import ilog.concert.IloException;
import problem.RandomProblem;
import solver.QP;
import solver.Solver;
import solver.Target;

/**
 * Decoupled investigates the solution of an optimization problem posed on
 * OR Stack Exchange.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Decoupled {
  private static final double MSTOSEC = 0.001;  // conversion factor

  /**
   * Dummy constructor.
   */
  private Decoupled() { }

  /**
   * Creates and attempts to solve a test problem.
   * @param args the command line arguments (ignored)
   */
  @SuppressWarnings({ "checkstyle:magicnumber" })
  public static void main(final String[] args) {
    // Select a seed for the random number generator.
    long seed = 1357;
    // Set dimensions for the problem.
    int nX = 50;  // # of X variables
    int nY = 75;  // # of Y variables
    int minC = 100;  // lower bound on constraint count
    int maxC = 200;  // upper bound on constraint count
    // Construct a problem instance.
    Problem prob = new RandomProblem(nX, nY, minC, maxC, seed);
//    Problem prob = new Dusto();
    System.out.println("Seed used = " + seed + ".");
    System.out.println(prob);
    // Construct a solver instance and try to solve the problem.
    long start = System.currentTimeMillis();
    try (Solver solver = new Solver(prob)) {
      System.out.println("Running the LP heuristic.");
      double value = solver.solve();
      System.out.println("Final objective value = " + value);
      System.out.println("Total run time = "
                         + MSTOSEC * (System.currentTimeMillis() - start)
                         + " seconds.");
      double[] x = solver.getSolution(true);
      double[] y = solver.getSolution(false);
//      System.out.println("Final solution for X:\n" + Arrays.toString(x));
//      System.out.println("Final solution for Y:\n" + Arrays.toString(y));
      // Sanity check.
      System.out.println("Recomputed objective value = " + prob.evaluate(x, y));
    } catch (IloException ex) {
      System.out.println("\nThe heuristic failed!\n" + ex.getMessage());
    }
    // Now try a QP model.
    start = System.currentTimeMillis();
    try (QP qp = new QP(prob)) {
      double timeLimit = 300;  // run time limit
      System.out.println("\nRunning the quadratic program.");
      double value = qp.solve(Target.GLOBAL, timeLimit);
      System.out.println("Total run time = "
                         + MSTOSEC * (System.currentTimeMillis() - start)
                         + " seconds.");
      System.out.println("Final objective value = " + value);
//      double[] x = qp.getSolution(true);
//      double[] y = qp.getSolution(false);
//      System.out.println("Final solution for X:\n" + Arrays.toString(x));
//      System.out.println("Final solution for Y:\n" + Arrays.toString(y));
    } catch (IloException ex) {
      System.out.println("\nThe QP model failed!\n" + ex.getMessage());
    }
  }

}
