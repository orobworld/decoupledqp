package problem;

/**
 * Dusto is an example of non-global convergence posted on the Discord OR
 * server by user "Dusto".
 *
 * The global optimum has a negative objective value, but the alternating
 * LP approach starts at the origin and gets stuck there.
 *
 * Link: https://discord.com/channels/888822916186243142/888897947650097162
 * Image of instance data: https://media.discordapp.net/attachments/
 * 888897947650097162/950936212028407839/unknown.png
 *
 * Note: because the problem is unconstrained other than bounds on the
 * variables, we treat the variable upper bounds as explicit constraints,
 * just so that the solvers don't trip over empty constraint matrices.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public class Dusto extends Problem {

  /**
   * Constructor.
   */
  @SuppressWarnings({ "checkstyle:magicnumber" })
  public Dusto() {
    // Set the problem dimensions.
    dimX = 3;
    dimY = 3;
    nConX = 3;
    nConY = 3;
    // Fill in the constraint data.
    lhsX = new double[][] {{-1, 0, 0}, {0, -1, 0}, {0, 0, -1}};
    lhsY = lhsX;
    rhsX = new double[] {-10, -10, -10};
    rhsY = rhsX;
    // Fill in the objective coefficients.
    objX = new double[] {0.0568, 0.5825, 0.8601};
    objY = new double[] {0.437, 0.745, 0.574};
    objXY = new double[][] {{-0.9247, 0.0541, 0.405},
                            {-0.3267, -0.316, 0.3994},
                            {-0.1206, -0.407, 0.443}};
    objXYt = new double[3][3];
    for (int i = 0; i < dimX; i++) {
      for (int j = 0; j < dimY; j++) {
        objXYt[j][i] = objXY[i][j];
      }
    }
  }
}
