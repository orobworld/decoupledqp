package problem;

import problem.Problem;
import java.util.Random;

/**
 * RandomProblem provides a random problem instance.
 *
 * Instances are constructed to be feasible, with nonnegative variables
 * and with all constraints expressed as >= inequalities.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class RandomProblem extends Problem {

  /**
   * Constructor.
   * @param nX the X dimension
   * @param nY the Y dimension
   * @param minC the minimum number of constraints for either subproblem
   * @param maxC the maximum number of constraints for either subproblem
   * @param seed the seed for the random number generator
   */
  public RandomProblem(final int nX, final int nY, final int minC,
                       final int maxC, final long seed) {
    // Set up and seed the random number generator.
    Random rng = new Random(seed);
    // Set the problem dimensions.
    dimX = nX;
    dimY = nY;
    nConX = rng.nextInt(minC, maxC);
    nConY = rng.nextInt(minC, maxC);
    // Allocate storage.
    lhsX = new double[nConX][dimX];
    rhsX = new double[nConX];
    lhsY = new double[nConY][dimY];
    rhsY = new double[nConY];
    objXY = new double[dimX][dimY];
    objXYt = new double[dimY][dimX];
    // Fill the constraint matrices with random values between -1 and +1.
    for (int i = 0; i < nConX; i++) {
      lhsX[i] = rng.doubles(dimX, -1, 1).toArray();
    }
    for (int i = 0; i < nConY; i++) {
      lhsY[i] = rng.doubles(dimY, -1, 1).toArray();
    }
    // Objective coeffients are nonnegative and, without loss of generality,
    // less than one.
    objX = rng.doubles(dimX).toArray();
    objY = rng.doubles(dimY).toArray();
    for (int i = 0; i < dimX; i++) {
      objXY[i] = rng.doubles(dimY).toArray();
    }
    for (int i = 0; i < dimX; i++) {
      for (int j = 0; j < dimY; j++) {
        objXYt[j][i] = objXY[i][j];
      }
    }
    // Before setting right-hand sides, we generate a random point in the
    // interior of each feasible region.
    double[] feasibleX = rng.doubles(dimX, 0, getUB()).toArray();
    double[] feasibleY = rng.doubles(dimY, 0, getUB()).toArray();
    // We evaluate each constraint at the feasible point of the corresponding
    // problem, then set the right-hand side to a value less than that.
    for (int i = 0; i < nConX; i++) {
      double z = dot(lhsX[i], feasibleX);
      double r = rng.nextDouble();
      rhsX[i] = (z < 0) ? (1 + r) * z : r * z;
    }
    for (int i = 0; i < nConY; i++) {
      double z = dot(lhsY[i], feasibleY);
      double r = rng.nextDouble();
      rhsY[i] = (z < 0) ? (1 + r) * z : r * z;
    }
  }

}
