package problem;

import java.util.Arrays;

/**
 * Problem is a container for a test problem.
 *
 * The nature of the problem is described in a question on OR Stack Exchange:
 * https://or.stackexchange.com/questions/7806/how-to-handle-a-non-
 * separable-bilinear-objective-function-in-the-special-case-of
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public class Problem {
  private static final double UB = 10;  // upper bound for X and Y

  protected double[][] lhsX;    // constraint matrix for the X subproblem
  protected double[][] lhsY;    // constraint matrix for the Y subproblem
  protected double[] rhsX;      // constraint limits for the X subproblem
  protected double[] rhsY;      // constraint limits for the Y subproblem
  protected double[] objX;      // linear objective coefficients for X
  protected double[] objY;      // linear objective coefficients for Y
  protected double[][] objXY;   // bilinear objective coefficients
  protected double[][] objXYt;  // transpose of objXY (for convenience)
  protected int dimX;           // dimension of X variable vector
  protected int dimY;           // dimension of Y variable vector
  protected int nConX;          // number of constraints on X
  protected int nConY;          // number of constraints on Y

  /**
   * Dummy constructor.
   * Subproblem constructors must do all the work.
   */
  public Problem() { }

  /**
   * Computes the inner product of two vectors.
   * @param a the first vector
   * @param b the second vector
   * @return the inner product
   */
  protected final double dot(final double[] a, final double[] b) {
    double sum = 0;
    for (int i = 0; i < a.length; i++) {
      sum += a[i] * b[i];
    }
    return sum;
  }

  /**
   * Gets the constraint matrix.
   * @param isX true if the X problem is targeted, false if the Y problem is
   * @return the constraint matrix
   */
  public final double[][] getLHS(final boolean isX) {
    return isX ? lhsX : lhsY;
  }

  /**
   * Gets the right hand side vector.
   * @param isX true if the X problem is targeted, false if the Y problem is
   * @return the right hand side vector
   */
  public final double[] getRHS(final boolean isX) {
    return isX ? rhsX : rhsY;
  }

  /**
   * Gets the linear objective coefficient vector.
   * @param isX true if the X problem is targeted, false if the Y problem is
   * @return the linear objective coefficients
   */
  public final double[] getObj(final boolean isX) {
    return isX ? objX : objY;
  }

  /**
   * Gets the matrix of bilinear coefficients.
   * @return the matrix of bilinear coefficients
   */
  public final double[][] getObjXY() {
    return objXY;
  }

  /**
   * Gets the number of variables.
   * @param isX true if the X problem is targeted, false if the Y problem is
   * @return the number of variables
   */
  public final int getDim(final boolean isX) {
    return isX ? dimX : dimY;
  }

  /**
   * Gets the number of constraints.
   * @param isX true if the X problem is targeted, false if the Y problem is
   * @return the number of constraints
   */
  public final int getnCons(final boolean isX) {
    return isX ? nConX : nConY;
  }

  /**
   * Computes the objective coefficient vector of one LP given a solution to
   * the other LP.
   * @param other the solution to the other LP
   * @param isX true if we want the coefficients for X, false if for Y
   * @return the coefficient vector for the target LP
   */
  public final double[] getCofficients(final double[] other,
                                       final boolean isX) {
    if (isX) {
      // Find the coefficient vector for the X problem assuming that "other"
      // is the solution to the Y problem.
      double[] c = Arrays.copyOf(objX, dimX);
      for (int i = 0; i < dimX; i++) {
        c[i] += dot(objXY[i], other);
      }
      return c;
    } else {
      // Find the coefficient Vector for the Y problem assuming that "other"
      // is the solution to the X problem.
      double[] c = Arrays.copyOf(objY, dimY);
      for (int j = 0; j < dimY; j++) {
        c[j] += dot(objXYt[j], other);
      }
      return c;
    }
  }

  /**
   * Evaluates the linear objective terms for a solution.
   * @param solution the solution to evaluate
   * @param isX true if this is X, false if Y
   * @return the value of the linear objective terms (only) for this solution
   */
  public final double linearSum(final double[] solution, final boolean isX) {
    return isX ? dot(objX, solution) : dot(objY, solution);
  }

  /**
   * Evaluates the objective function (for a sanity check).
   * @param x the X solution
   * @param y the Y solution
   * @return the overall objective value
  */
  public final double evaluate(final double[] x, final double[] y) {
    double sum = 0;
    for (int i = 0; i < dimX; i++) {
      sum += objX[i] * x[i];
    }
    for (int i = 0; i < dimY; i++) {
      sum += objY[i] * y[i];
    }
    for (int i = 0; i < dimX; i++) {
      for (int j = 0; j < dimY; j++) {
        sum += objXY[i][j] * x[i] * y[j];
      }
    }
    return sum;
  }

  /**
   * Generates a string summarizing the problem.
   * @return a summary of the problem
   */
  @Override
  public final String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("X problem has ").append(dimX).append(" variables and ")
      .append(nConX).append(" constraints.\n")
      .append("Y problem has ").append(dimY).append(" variables and ")
      .append(nConY).append(" constraints.\n");
    return sb.toString();
  }

  /**
   * Gets the upper bound on all variables.
   * @return the variable upper bound
   */
  public final double getUB() {
    return UB;
  }
}
