package solver;

import problem.Problem;
import ilog.concert.IloException;
import ilog.concert.IloNumVar;
import ilog.concert.IloObjective;
import ilog.cplex.IloCplex;

/**
 * LP implements a linear program for one set of variables, holding the
 * other set of variables fixed.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class LP implements AutoCloseable {
  private final Problem problem;       // the parent problem
  private final boolean isX;           // is this the X subproblem
  private final int nCons;             // number of constraints
  private final int nVars;             // number of variables

  private final IloCplex lp;           // the LP instance
  private final IloNumVar[] vars;      // the variables
  private IloObjective obj;            // the objective function

  /**
   * Constructor.
   * @param prob the parent problem
   * @param x true if this is the X problem, false if this is the Y problem
   * @throws IloException if the LP cannot be built
   */
  public LP(final Problem prob, final boolean x) throws IloException {
    problem = prob;
    isX = x;
    // Initialize the CPLEX instance.
    lp = new IloCplex();
    // Get the problem dimensions, constraint matrix and RHS vector.
    nCons = problem.getnCons(isX);
    nVars = problem.getDim(isX);
    double[][] lhs = problem.getLHS(isX);
    double[] rhs = problem.getRHS(isX);
    // Create the variables.
    vars = lp.numVarArray(nVars, 0, problem.getUB());
    // Set up the constraints.
    for (int i = 0; i < nCons; i++) {
      lp.addGe(lp.scalProd(vars, lhs[i]), rhs[i]);
    }
    // Create a zero objective vector for now.
    obj = lp.addMinimize();
    // Suppress any output.
    lp.setOut(null);
  }

  /**
   * Updates the objective function.
   * @param coeffs the new objective coefficients
   * @throws IloException if CPLEX blows up
   */
  public void setObjective(final double[] coeffs) throws IloException {
    lp.remove(obj);
    obj = lp.addMinimize(lp.scalProd(vars, coeffs));
  }

  /**
   * Solves the LP.
   * @return the optimal objective value
   * @throws IloException if the LP is not solved to optimality
   */
  public double solve() throws IloException {
    lp.solve();
    if (lp.getStatus() == IloCplex.Status.Optimal) {
      return lp.getObjValue();
    } else {
      throw new IloException("LP solve failed with status " + lp.getStatus());
    }
  }

  /**
   * Gets the solution to the current LP.
   * @return the solution
   * @throws IloException if no solution is available
   */
  public double[] getSolution() throws IloException {
    return lp.getValues(vars);
  }

  /**
   * Closes the LP.
   */
  @Override
  public void close() {
    lp.end();
  }
}
