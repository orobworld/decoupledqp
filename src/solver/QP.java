package solver;

import problem.Problem;
import ilog.concert.IloException;
import ilog.concert.IloLQNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;
import java.util.Arrays;

/**
 * QP implements a quadratic programming model for the original problem
 * (combining both X and Y).
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class QP implements AutoCloseable {
  private final IloCplex qp;           // the QP instance
  private final IloNumVar[] vars;      // the variables
  private final int nX;                // X dimension
  private final int nY;                // Y dimension

  /**
   * Constructor.
   * @param prob the problem to solve
   * @throws IloException if the quadratic programming model cannot be built
   */
  public QP(final Problem prob) throws IloException {
    // Initialize the model instance.
    qp = new IloCplex();
    // Get dimensions.
    nX = prob.getDim(true);
    nY = prob.getDim(false);
    int nConX = prob.getnCons(true);
    int nConY = prob.getnCons(false);
    // Create separate X and Y variable arrays to simplify model construction.
    IloNumVar[] x = qp.numVarArray(nX, 0, prob.getUB());
    IloNumVar[] y = qp.numVarArray(nY, 0, prob.getUB());
    // Combine them into a global array.
    vars = Arrays.copyOf(x, nX + nY);
    System.arraycopy(y, 0, vars, nX, nY);
    // Add the constraints.
    double[][] lhs = prob.getLHS(true);
    double[] rhs = prob.getRHS(true);
    for (int i = 0; i < nConX; i++) {
      qp.addGe(qp.scalProd(x, lhs[i]), rhs[i]);
    }
    lhs = prob.getLHS(false);
    rhs = prob.getRHS(false);
    for (int i = 0; i < nConY; i++) {
      qp.addGe(qp.scalProd(y, lhs[i]), rhs[i]);
    }
    // Add the objective function.
    IloLQNumExpr obj = qp.lqNumExpr();
    obj.add(qp.scalProd(x, prob.getObj(true)));
    obj.add(qp.scalProd(y, prob.getObj(false)));
    double[][] c = prob.getObjXY();
    for (int i = 0; i < nX; i++) {
      for (int j = 0; j < nY; j++) {
        obj.addTerm(c[i][j], x[i], y[j]);
      }
    }
    qp.addMinimize(obj);
  }

  /**
   * Solves the quadratic program.
   * @param target the optimality target
   * @param timeLimit the run time limit (in seconds)
   * @return the final objective value
   * @throws IloException if CPLEX blows up
   */
  public double solve(final Target target, final double timeLimit)
                      throws IloException {
    // Set the optimality target and time limit.
    qp.setParam(IloCplex.Param.OptimalityTarget, target.value());
    qp.setParam(IloCplex.DoubleParam.TimeLimit, timeLimit);
    // Try to solve the model.
    qp.solve();
    System.out.println("QP final status = " + qp.getStatus());
    return qp.getObjValue();
  }

  /**
   * Closes the QP instance.
   */
  @Override
  public void close() {
    qp.end();
  }

  /**
   * Gets the solution vector.
   * @param isX if true, get X, else get Y
   * @return the solution vector
   * @throws IloException if the solution does not exist
   */
  public double[] getSolution(final boolean isX) throws IloException {
    double[] x = qp.getValues(vars);
    if (isX) {
      return Arrays.copyOfRange(x, 0, nX);
    } else {
      return Arrays.copyOfRange(x, nX, nX + nY);
    }
  }

}
