package solver;

/**
 * Target enumerates possible targets for optimization of the quadratic program.
 *
 * The CPLEX target "OPTIMALCONVEX" is skipped as the objective is not convex.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public enum Target {
  /** AUTO lets CPLEX decide. */
  AUTO(0),
  /** FIRSTORDER looks for solutions (not necessarily global optima) satisfying
   * first order conditions.
   */
  FIRSTORDER(2),
  /** GLOBAL looks for a global optimum, possibly turning the problem into
   * a MIQP.
   */
  GLOBAL(3);

  private final int value;  // the CPLEX value for the target.

  /**
   * Constructor.
   * @param v the value used by CPLEX
   */
  Target(final int v) {
    value = v;
  }

  /**
   * Gets the numeric value of the option.
   * @return the numeric value
   */
  public int value() {
    return value;
  }
}
