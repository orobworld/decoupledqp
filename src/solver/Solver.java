package solver;

import problem.Problem;
import ilog.concert.IloException;

/**
 * Solver implements an algorithm to solve the bilinear problem by alternating
 * between linear programs, one holding X fixed and the other holding Y fixed.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Solver implements AutoCloseable {
  private final Problem problem;  // the parent problem
  private final LP xLP;           // LP model for the X problem
  private final LP yLP;           // LP model for the Y problem
  private double[] x;             // incumbent X solution
  private double[] y;             // incumbent Y solution

  /**
   * Constructor.
   * @param prob the problem to solve
   * @throws IloException if either LP model cannot be constructed
   */
  public Solver(final Problem prob) throws IloException {
    problem = prob;
    // Set up the X and Y linear programs.
    xLP = new LP(problem, true);
    yLP = new LP(problem, false);
  }

  /**
   * Solves the problem by solving alternating LPs.
   * @return the final objective value
   * @throws IloException if any LP solve fails
   */
  public double solve() throws IloException {
    // Solve the X problem without the bilinear terms to get an initial
    // X incumbent.
    xLP.setObjective(problem.getCofficients(new double[problem.getDim(false)],
                                            true));
    xLP.solve();
    x = xLP.getSolution();
    double xsum = problem.linearSum(x, true);
    // Solve the Y problem using the X solution and report the value of the
    // combined solution.
    yLP.setObjective(problem.getCofficients(x, false));
    double incumbentValue = xsum + yLP.solve();
    y = yLP.getSolution();
    double ysum = problem.linearSum(y, false);
    System.out.println("\nInitial incumbent value = " + incumbentValue);
    boolean next = true;  // flags next problem to solve (true -> X)
    // Alternate solving LPs until no improvement occurs.
    while (true) {
      if (next) {
        // Solve for X given Y.
        xLP.setObjective(problem.getCofficients(y, true));
        double xval = xLP.solve();
        // Check for improvement.
        if (xval + ysum < incumbentValue) {
          incumbentValue = xval + ysum;
          x = xLP.getSolution();
          xsum = problem.linearSum(x, true);
          System.out.println("... After X problem, incumbent value = "
                             + incumbentValue);
        } else {
          System.out.println("Terminating: X LP produced no improvement.");
          break;
        }
      } else {
        // Solve for Y given X.
        yLP.setObjective(problem.getCofficients(x, false));
        double yval = yLP.solve();
        // Check for improvement.
        if (yval + xsum < incumbentValue) {
          incumbentValue = yval + xsum;
          y = yLP.getSolution();
          ysum = problem.linearSum(y, false);
          System.out.println("... After Y problem, incumbent value = "
                             + incumbentValue);
        } else {
          System.out.println("Terminating: Y LP produced no improvement.");
          break;
        }
      }
      next = !next;
    }
    return incumbentValue;
  }

  /**
   * Gets the solution vector.
   * @param isX if true, get X, else get Y
   * @return the solution vector
   */
  public double[] getSolution(final boolean isX) {
    return isX ? x : y;
  }

  /**
   * Closes the solver instance.
   */
  @Override
  public void close() {
    xLP.close();
    yLP.close();
  }
}
